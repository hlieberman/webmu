/*
   This is part of WebMU.

   Copyright 2016-2017, Harlan Lieberman-Berg <hlieberman@setec.io>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
window.webmu = (function() {
    var mainbox;
    var inputbox;
    var conn;
    var timer;
    var timer_flag;
    var history = [];
    var historyPos = 0;
    var dirtyHistory;

    function init() {
	// Clear any other error boxes that might be present
	var errs = document.getElementsByClassName("error-box");
	while (errs.length > 0) {
	    errs[0].parentNode.removeChild(errs[0]);
	}

	var proto = identify_protocol();
	conn = new WebSocket(proto + window.location.host + "/mu?" + get_sessionid());
	conn.onerror = handle_error;
	conn.onmessage = process_message;
	conn.onclose = handle_hangup;
	conn.onopen = handle_open;
	mainbox = document.getElementById("chat-container");
	inputbox = document.getElementById("input-box");
	inputbox.onkeypress = pose_input;
	timer = window.setInterval(send_ping, 7500);
	var n = Notification.requestPermission();
	if (n !== undefined) {
	    n.then(function(r) {
		console.log("Permission to notify: " + r);
	    });
	}
    }

    function display_error(errmsg) {
	var eb = document.createElement("div");
	eb.className = "error-box";
	eb.textContent = errmsg;
	mainbox.appendChild(eb);
	mainbox.lastChild.scrollIntoView(true);
	trigger_notification(errmsg);
    };

    function get_sessionid() {
	var sess = window.sessionStorage.getItem("sess");
	if (sess) {
	    return "sess=" + encodeURIComponent(sess);
	}
	return "";
    };

    function handle_error(err) {
	console.log("Error! " + JSON.stringify(err));
	window.clearInterval(timer);
	display_error("Something has gone wrong!  To report this error, email " +
		      "poly@xmenrevolution.com with the contents of the Javascript console.");
    };

    function handle_open(evt) {
	var a = document.createElement("div");
	a.textContent = "Connected!";
	mainbox.appendChild(a);
	mainbox.lastChild.scrollIntoView(true);
    }

    function handle_hangup(evt) {
	console.log("Hangup: " + JSON.stringify(evt));
	window.clearInterval(timer);
	var eb = document.createElement("div");
	eb.className = "error-box";
	eb.innerHTML = 'The connection to the server has gone away. <a onclick="window.webmu.init()" href="#">Reconnect?</a>';
	mainbox.appendChild(eb);
	mainbox.lastChild.scrollIntoView(true);
	trigger_notification("The connection to the server has gone away!");
    }

    function identify_protocol() {
	if (location.protocol == "https:") {
	    return "wss://";
	} else {
	    return "ws://";
	}
    }

    function process_message(event) {
	try {
	    var msg = JSON.parse(event.data);
	}
	catch (e) {
	    console.log("Non-JSON data! " + JSON.stringify(event));
	}

	if (msg.pose) {
	    trigger_notification(msg.pose);
	    var a = document.createElement("div");
	    a.innerHTML = render_pose(msg.pose);
	    a.title = new Date().toLocaleString();
	    mainbox.appendChild(a);
	    mainbox.lastChild.scrollIntoView(true);
	} else if (msg.type == "pong") {
	    timer_flag++;
	} else if (msg.type == "sess") {
	    window.sessionStorage.setItem("sess", msg.id);
	} else {
	    console.log("Got unknown command: " + msg);
	}
    }

    function trigger_notification(pose) {
	if (document.hidden) {
	    var n = {
		body: ansi_up.ansi_to_text(pose.trim())
	    };
	    new Notification("MUSH Activity", n);

	}
    }

    function render_pose(pose) {
	return ansi_up.ansi_to_html(ansi_up.escape_for_html(pose));
    }

    function pose_input(e) {
	if (!e) e = window.event;
	switch (e.key) {
	case "Enter":
	    send_pose(inputbox.value);
	    history.push(inputbox.value);
	    historyPos = history.length;
	    dirtyHistory = undefined;
	    inputbox.value = '';
	    break;
	case "ArrowUp":
	    if (inputbox.value.trim() && historyPos == history.length) {
		dirtyHistory = inputbox.value;
	    }
	    if (historyPos > 0) {
		historyPos--;
		inputbox.value = history[historyPos];
	    }
	    break;
	case "ArrowDown":
	    if (historyPos < history.length) {
		historyPos++;
		inputbox.value = history[historyPos] || dirtyHistory || "";
	    }
	    break;
	}
    }

    function send_pose(pose) {
	var json = JSON.stringify({"pose": pose});

	try {
	    conn.send(json);
	}
	catch (e) {
	    handle_error(e);
	}
    }

    function send_ping() {
	var json = JSON.stringify({"type": "ping"});

	if (timer_flag >= 3) {
	    handle_hangup("Pong Missed");
	}

	try {
	    conn.send(json);
	}
	catch (e) {
	    handle_error(e);
	}
    }

    init();

    return {
	init: init,
    };
})();
