PROJECT = webmu
PROJECT_DESCRIPTION = A web interface to a MU*
PROJECT_VERSION = 1.1.0

ERLC_OPTS += +'{parse_transform, lager_transform}'

DEPS = cowboy jiffy lager uuid
LOCAL_DEPS = runtime_tools

dep_cowboy_commit = master

include erlang.mk
