# WebMU

WebMU is a MU* to WebSocket proxy, written in Erlang and Javascript.
It can be run using the built-in high-performance web server, or
behind an nginx proxy.

WebMU is written to serve a single MU* at a time; though it would be
relatively trivial to convert this to a client that can be used to
connect to arbitrary MU*'s, this has intentionally not been done.
WebMU is intended to allow people unfamiliar with MU*'s easy access
without the need to download client software or use telnet, rather
than as a replacement for more powerful MU* specific clients.

# Licenses
Copyright 2016, Harlan Lieberman-Berg <hlieberman@setec.io>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


## Incorporated Software
This program includes ansi_up in priv/static/ansi_up.js.  ansi_up is
licensed under the terms of the MIT License:

Copyright 2011-2015, Dru Nelson and contributors.

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.