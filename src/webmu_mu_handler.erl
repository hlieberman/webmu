%% This is part of WebMU.
%%
%% Copyright 2016-2017, Harlan Lieberman-Berg <hlieberman@setec.io>
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU Affero General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU Affero General Public License for more details.
%%
%% You should have received a copy of the GNU Affero General Public License
%% along with this program.  If not, see <http://www.gnu.org/licenses/>.

-module(webmu_mu_handler).
-behaviour(gen_server).

-export([start/1]).

%% gen_server.
-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).

-record(state, {
	  buffer = <<"">> :: iodata(),
	  id :: iodata(),
	  sesstable :: ets:tid(),
	  socket :: gen_tcp:socket(),
	  tref :: timer:tref(),
	  wshandler :: pid() | 'no_ws'
}).


start(Args) ->
    gen_server:start(?MODULE, Args, []).

%% gen_server.
init([Id, TabId, Ws]) ->
    ets:insert(TabId, {Id, self()}),
    monitor(process, Ws),
    {ok, Sock} = gen_tcp:connect(get_env(mu_host), get_env(mu_port),
				 [{active, true}, {mode, binary}]),
    lager:debug("Connected"),
    {ok, #state{id = Id, sesstable = TabId, socket = Sock, wshandler = Ws}}.

handle_call(transfer, {Ws,_Tag}, #state{tref = T, socket = Socket} = State) ->
    lager:debug("Transferring to ~p", [Ws]),
    _ = timer:cancel(T),
    monitor(process, Ws),
    inet:setopts(Socket, [{active, true}]),
    {reply, ok, State#state{wshandler = Ws}};
handle_call(_Request, _From, State) ->
    {reply, ignored, State}.

handle_cast({pose, Pose}, #state{socket = Socket} = State) ->
    gen_tcp:send(Socket, [Pose,"\r\n"]),
    {noreply, State};
handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info({'DOWN', _Ref, process, _Pid, _Reason}, #state{socket = Socket} = State) ->
    lager:debug("WS link died"),
    inet:setopts(Socket, [{active, false}]),
    {ok, TRef} = timer:send_after(300000, sess_timeout), % 300000 ms = 5 minutes
    {noreply, State#state{tref = TRef, wshandler = no_ws}};
handle_info({tcp_closed, _Socket}, State) ->
    lager:debug("MU connection died."),
    {stop, normal, State};
handle_info(sess_timeout, State) ->
    lager:debug("Timed out waiting for a reconnect."),
    {stop, normal, State};
handle_info({tcp, _Socket, <<255, 253, 34>>}, State) ->
    %% Ignore telnet handshake
    {noreply, State};
handle_info({tcp, _Socket, Data}, State) ->
    handle_mu_message(Data, binary:last(Data), State);
handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, #state{sesstable = TabId, id = Id}) ->
    ets:delete(TabId, Id),
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.


%% Private Functions
handle_mu_message(Data, 10, #state{buffer = Buffer, wshandler = Ws} = State) ->
    Pose = erlang:iolist_to_binary([Buffer, Data]),
    lager:debug("respondingggg with ~p~n to ~p", [Pose, Ws]),
    Ws ! {pose, Pose},
    {noreply, State#state{buffer = <<"">>}};
handle_mu_message(Data, _Last, #state{buffer = Buffer} = State) ->
    lager:debug("No newline =( ~p~n", [Data]),
    {noreply, State#state{buffer = [Buffer, Data]}}.

get_env(Param) ->
    {ok, Val} = application:get_env(webmu, Param),
    Val.
