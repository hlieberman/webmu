%% This is part of WebMU.
%%
%% Copyright 2016, Harlan Lieberman-Berg <hlieberman@setec.io>
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU Affero General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU Affero General Public License for more details.
%%
%% You should have received a copy of the GNU Affero General Public License
%% along with this program.  If not, see <http://www.gnu.org/licenses/>.

-module(webmu_app).
-behaviour(application).

-export([start/2]).
-export([stop/1]).

start(_Type, _Args) ->
    application:start(cowboy),
    TabId = ets:new(sesstable, [set, public, named_table]),
    Dispatch = cowboy_router:compile([{'_', [
					     {"/mu", webmu_ws_handler, [TabId]},
					     {"/", cowboy_static, {priv_file, webmu, "static/index.html"}},
					     {"/assets/[...]", cowboy_static, {priv_dir, webmu, "static"}}
				     ]}]),
    {ok, _} = cowboy:start_clear(mu_handler, [{port, get_env(http_port)}], #{
						   env => #{dispatch => Dispatch}
						  }),
    webmu_sup:start_link().

stop(_State) ->
	ok.

get_env(Param) ->
    {ok, Val} = application:get_env(webmu, Param),
    Val.
