%% This is part of WebMU.
%%
%% Copyright 2016-2017, Harlan Lieberman-Berg <hlieberman@setec.io>
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU Affero General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU Affero General Public License for more details.
%%
%% You should have received a copy of the GNU Affero General Public License
%% along with this program.  If not, see <http://www.gnu.org/licenses/>.

-module(webmu_ws_handler).

-export([init/2]).
-export([websocket_init/1]).
-export([websocket_handle/2]).
-export([websocket_info/2]).
-export([websocket_terminate/2]).

-record(state, {
	  id :: iodata(),
	  sesstable :: ets:tid(),
	  muhandler :: pid(),
	  ping_flag = false :: boolean()
}).

init(Req, [TabId]) ->
    % Time the connection out after 60,000 milliseconds (1 min)
    #{sess := Id} = cowboy_req:match_qs([{sess, [], undefined}], Req),
    {cowboy_websocket, Req, #state{id = Id, sesstable = TabId}, #{idle_timeout => 60000}}.

websocket_init(#state{id = Id, sesstable = TabId} = State) ->
    lager:debug("init"),
    {ok, Sess} = check_session(Id, TabId),
    monitor(process, Sess),
    timer:send_after(10000, ping_check),
    {ok, State#state{muhandler = Sess}}.

websocket_handle({text, Data}, #state{muhandler = Mu} = State) ->
    lager:debug("ooh, data ~p~n", [Data]),
    case catch json_decode(Data) of
	#{<<"pose">> := Msg} ->
	    gen_server:cast(Mu, {pose, Msg}),
	    {ok, State};
	#{<<"type">> := <<"ping">>} ->
	    {reply, {text, <<"{\"type\"\:\"pong\"}">>}, State#state{ping_flag = false}};
	_ ->
	    {ok, State}
    end;
websocket_handle(Frame, State) ->
    lager:debug("what ~p~n", [Frame]),
    {ok, State}.

websocket_info({pose, Pose}, State) ->
    lager:debug("Got pose!"),
    JSON = jiffy:encode(#{<<"pose">> => Pose}),
    {reply, {text, JSON}, State};
websocket_info({sessid, Id}, State) ->
    JSON = jiffy:encode(#{<<"type">> => <<"sess">>, <<"id">> => Id}),
    {reply, {text, JSON}, State#state{id = Id}};
websocket_info(ping_check, #state{ping_flag = Ping} = State) ->
    case Ping of
	true ->
	    {stop, State};
	false ->
	    {ok, State#state{ping_flag = true}}
    end;
websocket_info({'DOWN', _, process, _From, _Reason}, State) ->
    {stop, State};
websocket_info(Info, State) ->
    lager:debug("aaaaa ~p~n", [Info]),
    {ok, State}.

websocket_terminate(_Reason, _State) ->
    ok.

%%
%% Private Functions
%%

check_session(undefined, TabId) ->
    create_session(TabId);
check_session(Id, TabId) ->
    %% The client is trying to reconnect.  Check if we have a live session.
    lager:debug("Looking up session ~p", [Id]),
    case ets:lookup(TabId, Id) of
	[] ->
	    lager:debug("Dead session. =("),
	    create_session(TabId);
	[{_Id, Pid}] ->
	    lager:debug("Got a live one!"),
	    ok = gen_server:call(Pid, transfer),
	    {ok, Pid}
    end.

create_session(TabId) ->
    quickrand:seed(),
    Id = list_to_binary(uuid:uuid_to_string(uuid:get_v4_urandom())),
    self() ! {sessid, Id},
    supervisor:start_child(webmu_sup, [[Id, TabId, self()]]).

json_decode(JSON) ->
    jiffy:decode(JSON, [return_maps]).
